import { FaTwitter, FaFacebookF, FaInstagram, FaLinkedinIn } from 'react-icons/fa';
import Link from 'next/link';
import Image from 'next/image';
import logo from '../../../public/images/LOGO.png';
import ButtonBottom from './components/button-bottom';
import { MdOutlineKeyboardDoubleArrowRight, MdOutlineEmail, MdOutlinePhone } from 'react-icons/md';
import { SlLocationPin } from 'react-icons/sl';

const socialLinks = [
  { href: '#', icon: FaTwitter },
  { href: '#', icon: FaFacebookF },
  { href: '#', icon: FaInstagram },
  { href: '#', icon: FaLinkedinIn }
];

const usefulLinks = [
  { href: '#', text: 'Home' },
  { href: '#', text: 'About' },
  { href: '#', text: 'Pricing' },
  { href: '#', text: 'Services' },
  { href: '#', text: 'Blog' }
];

const contactDetails = [
  { icon: MdOutlineEmail, text: 'No 10, Flower Road, Colombo 07' },
  { icon: SlLocationPin, text: '+94 112 333 666' },
  { icon: MdOutlinePhone, text: 'info@ZephyrCode.com' }
];

const Footer = () => {
  return (
    <div className="p-4 w-full bg-headercolor text-white">
      <div className='lg:grid grid-cols-4 gap-4 container border-b-2 border-white p-4'>
        <div className='lg:col-span-2 py-4'>
          <Link href={'./'}>
            <Image className='bg-white inset-0 p-2 rounded' width={100} alt='logo' src={logo} />
          </Link>
          <div className='py-4 leading-7 w-4/5'>
            Join with ZephyrCode and feel the difference. It helps you to scale your business with hassle-free integration & fast scaling facility.
          </div>
          <div className="flex gap-3 py-3">
            {socialLinks.map((link, index) => (
              <Link key={index} href={link.href}>
                <link.icon className="text-4xl p-2 border-2 border-white rounded-full" />
              </Link>
            ))}
          </div>
        </div>
        <div>
          <div className="font-bold uppercase text-lg py-4 tracking-widest">USEFUL LINKS</div>
          <div className='flex flex-col gap-4'>
            {usefulLinks.map((link, index) => (
              <div key={index} className='flex items-center gap-2'>
                <MdOutlineKeyboardDoubleArrowRight className='text-xl' />
                <Link href={link.href}>{link.text}</Link>
              </div>
            ))}
          </div>
        </div>
        <div>
          <div className="font-bold uppercase text-lg py-4 tracking-widest">CONTACT US</div>
          <div className='flex flex-col gap-4'>
            {contactDetails.map((detail, index) => (
              <div key={index} className='flex items-center gap-2'>
                <detail.icon className='text-xl' />
                {detail.text}
              </div>
            ))}
          </div>
        </div>
      </div>

      <div className="flex justify-between items-center p-4 container leading-6">
        <div>Copyright © 2024 Zephyr Code Pvt Ltd. All rights reserved.</div>
        <ButtonBottom />
      </div>
    </div>
  );
};

export default Footer;

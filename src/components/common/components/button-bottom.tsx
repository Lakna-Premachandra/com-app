'use client'
import React from 'react'
import { FaArrowCircleUp } from "react-icons/fa";

function ButtonBottom() {
  return (
    <>
      <button onClick={() => window.scrollTo({ top: 0, behavior: 'smooth' })}> <FaArrowCircleUp className="text-3xl" /> </button>
    </>
  )
}

export default ButtonBottom

'use client'
import { motion } from 'framer-motion'
import Image from 'next/image'
import figma from '../../../../public/images/figma.png'
import web from '../../../../public/images/programming.png'
import service1 from '../../../../public/images/service1.png'
import service3 from '../../../../public/images/service3.png'
import service4 from '../../../../public/images/service4.png'
import shopify from '../../../../public/images/shopify.png'


const services = [
    {
        src: service1,
        alt: "Web App Development",
        title: "Web Development",
        description: "Creating websites is easy, creating ones that convert and keep users is an art.",
        icon: web,
    },
    {
        src: service3,
        alt: "E-Commerce  Website Development",
        title: "E-Commerce",
        description: "Channel conversion, retargeting, and customer communication are key to success.",
        icon: shopify,
    },
    {
        src: service4,
        alt: "UI/UX Design",
        title: "UI/UX Design",
        description: "Armed with this knowledge,we craft of enchantment and a disire for more.",
        icon: figma
    },
];

function ServicesSection() {
    return (
        <>
            <div className=' my-10'>
                <div className=" ">
                    <div className=" container">
                        <div
                            className="py-6 grid justify-center sm:grid-cols-2  md:grid-cols-3  gap-8  ">
                            {services.map((service, index) => (
                                <div key={index}
                                    className=" text-slate-700 flex-col flex  border-slate-300  sx:flex items-center gap-4 rounded-xl cursor-pointer max-w-[400px] ">
                                    <motion.div
                                        className="text-5xl  mb-3"
                                        initial={{ scale: 0 }}
                                        animate={{ rotate: 360, scale: 1 }}
                                        transition={{
                                            type: "spring",
                                            stiffness: 200,
                                            damping: 20
                                        }}
                                    >
                                        <Image alt='image' width={60} src={service.icon} />
                                    </motion.div>
                                    <div className='flex-col flex items-center'>
                                        <div
                                            className="text-2xl font-semibold tracking-wide text-center">{service.title}</div>
                                        <div title={service.description}
                                            className=" text-sm  font-[300] px-6 tracking-wide text-center">{service.description}</div>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ServicesSection

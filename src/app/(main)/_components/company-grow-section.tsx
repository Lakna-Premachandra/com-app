import React from 'react';
import { FiUsers, FiAward } from "react-icons/fi";
import { TbClockBolt } from "react-icons/tb";
import { IoMdCheckmarkCircleOutline } from "react-icons/io";

const stats = [
  { icon: <FiUsers />,
    value: '60+',
    label: 'Satisfied Customers' 
  },
  { icon: <TbClockBolt />,
    value: '200+',
    label: 'Days Of Operation' 
  },
  { icon: <IoMdCheckmarkCircleOutline />,
    value: '6+',
    label: 'Countries Served'
  },
  { icon: <FiAward />,
    value: '3+', 
    label: 'Recognitions Earned'
  },
];

function CompanyGrowSection() {
  return (
    <div className="container px-4 sm:px-6 lg:px-8 py-12">
      <div className="text-4xl sm:text-8xl font-bold py-12">
        <span className="text-outline-blue uppercase">Our&nbsp;</span>
        <span className="text-headercolor uppercase">Company Growth</span>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-8">
        {stats.map((stat, index) => (
          <div
            key={index}
            className="bg-gray-200 p-8 rounded-xl text-center cursor-pointer hover:bg-headercolor hover:text-white"
          >
            <button className="text-4xl">{stat.icon}</button>
            <div className="text-6xl font-semibold py-6">{stat.value}</div>
            <div className="text-xl">{stat.label}</div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default CompanyGrowSection;

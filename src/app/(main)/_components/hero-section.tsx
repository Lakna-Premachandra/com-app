'use client'
import Image from "next/image";
import heroImage from '../../../../public/images/herosection.png';
import { FlipWords } from '../../../components/ui/flip-words';
import { BsArrowRightCircleFill } from "react-icons/bs";
import { Button } from "@/components/ui/button"
import { motion } from "framer-motion";


const words = ["modern", "better"];

const HeroSection = () => {
    return (
        <div className="pb-4 relative">
            {/* Rotated background with curved bottom using an SVG mask */}
            <div className="absolute inset-0 transform bg-gradient-to-b from-blue-500 to-blue-700" style={{ clipPath: 'url(#clipPath)' }}></div>

            <svg width="0" height="0">
                <defs>
                    <clipPath id="clipPath" clipPathUnits="objectBoundingBox">
                        <path d="M0,-1 H1 V0.9 C0.8,1 0.2,1 0,0.9 Z" />
                    </clipPath>

                </defs>
            </svg>

            <div className="lg:grid lg:grid-cols-2 justify-center items-center place-items-center pt-20 sm:pt-20 gap-4 container text-white relative pb-6">
                <div className="flex flex-col gap-4 sm:items-start items-center relative z-10">
                    <div className="sm:text-8xl text-3xl sm:text-left text-center font-bold text-white dark:text-[whitesmoke]">
                        Build
                        <FlipWords className="text-headings" words={words} />
                        websites with code
                    </div>
                    <button className="bg-headings px-5 py-2 sm:text-left font-medium text-center text-sm rounded-full border-none  text-slate-700 cursor-pointer ">
                        Contact us
                    </button>
                </div>
                <div className="relative z-10">
                    <Image alt="image" width={500} src={heroImage} />
                </div>
            </div>
        </div>
    );
};

export default HeroSection;

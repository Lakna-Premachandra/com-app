import Image from "next/image"
import image from '../../../../public/images/startups,-entrepreneurship-and-growth.png'
import image_1 from '../../../../public/images/business,-entrepreneurship-and-growth.png'
function Projects() {

  return (
    <>
      <div className="container my-10 ">
        <div className=" flex md:flex-row-reverse flex-col gap-4 items-center">
          <div className="flex flex-col gap-4 text-slate-800 ">
            <div className="text-3xl font-semibold">
              Beautifully crafted UI components, ready for your next project.
            </div>
            <div >
              Over 500+ professionally designed, fully responsive, expertly crafted component examples you can drop into your Tailwind projects and customize to your heart’s content.      Over 500+ professionally designed, fully responsive, expertly crafted component examples you can drop into your Tailwind projects and customize to your heart’s content.
            </div>
            <div className="text-headercolor">
              Browse all components →
            </div>
          </div>
          <div>
            <Image alt="image" src={image} className=" border-blue-400 min-w-[150px]" width={1800} />
          </div>
        </div>

        <div className=" flex md:flex-row flex-col md:m-0 my-2 gap-4 items-center">
          <div className="flex flex-col gap-4 text-slate-800  ">
            <div className="text-3xl font-semibold">
              Beautifully crafted UI components, ready for your next project.
            </div>
            <div >
              Over 500+ professionally designed, fully responsive, expertly crafted component examples you can drop into your Tailwind projects and customize to your heart’s content.      Over 500+ professionally designed, fully responsive, expertly crafted component examples you can drop into your Tailwind projects and customize to your heart’s content.
            </div>
            <div className="text-headercolor">
              Browse all components →
            </div>
          </div>
          <div>
            <Image alt="image" src={image_1} className=" border-blue-400 min-w-[150px]" width={1800} />
          </div>
        </div>
      </div>
    </>
  )
}

export default Projects

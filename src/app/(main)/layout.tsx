'use client'
import Header from "@/components/common/header";
import { useEffect, useState } from "react";
import Loader from "./loader/loader";


export default function MainLayout({
    children,
}: Readonly<{
    children: React.ReactNode;
}>) {
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        // Simulate loading time
        const timer = setTimeout(() => {
            setLoading(false);
        }, 2000); // Adjust the time as needed

        return () => clearTimeout(timer);
    }, []);

    if (loading) return <Loader />;
    return (
        <html lang="en">
            <body >
                <Header />
                <div className="ashen sam">
                    {children}
                </div>
            </body>
        </html>
    );
}

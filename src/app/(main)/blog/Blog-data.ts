"use client"

export interface BlogPost {
  id: number;
  title: string;
  content: string;
  author: string;
  date: string;
  image: string;
}

export const BlogData: BlogPost[] = [
  {
    id: 1,
    title: "Microservices Architecture",
    content:
      "Microservices architecture allows for the development of small, independent components that work together. Microservices architecture allows for the development of small, independent components that work together. Microservices architecture allows for the development of small, independent components that work together. ",
    author: "John Doe",
    date: "2023-01-15",
    image: "/images/microservices.jpg",
  },
  {
    id: 2,
    title: "Understanding RESTful APIs",
    content:
      "RESTful APIs are the backbone of modern web development. They allow different systems to communicate.Microservices architecture allows for the development of small, independent components that work together. Microservices architecture allows for the development of small, independent components that work together. Microservices architecture allows for the development of small, independent components that work together.",
    author: "Jane Smith",
    date: "2023-02-10",
    image: "/images/restful-api.jpg",
  },
  {
    id: 3,
    title: "Getting Started with TypeScript",
    content:
      "TypeScript is a powerful superset of JavaScript that adds static types, making your code more robust and maintainable.",
    author: "Alice Johnson",
    date: "2023-03-05",
    image: "/images/typescript.jpg",
  },
  {
    id: 4,
    title: "Deploying Applications with Docker",
    content:
      "Docker simplifies the process of deploying applications by containerizing them. This ensures consistency across environments.Microservices architecture allows for the development of small, independent components that work together. Microservices architecture allows for the development of small, independent components that work together.Microservices architecture allows for the development of small, independent components that work together. Microservices architecture allows for the development of small, independent components that work together.",
    author: "Bob Brown",
    date: "2023-04-20",
    image: "/images/docker.jpg",
  },
  {
    id: 5,
    title: "Introduction to Kubernetes",
    content:
      "Kubernetes is an open-source platform for managing containerized workloads and services. It provides a framework for automating deployment.",
    author: "Charlie Davis",
    date: "2023-05-15",
    image: "/images/kubernetes.jpg",
  },
  {
    id: 6,
    title: "Effective Agile Development Practices",
    content:
      "Agile development practices promote continuous delivery and flexibility to adapt to changing requirements. Learn how to implement Agile.",
    author: "Diana Evans",
    date: "2023-06-10",
    image: "/images/agile-development.jpg",
  },
  {
    id: 7,
    title: "Building Scalable Web Applications",
    content:
      "Scalability is crucial for web applications to handle growing amounts of traffic and data. This post explores strategies for building scalable.",
    author: "Ethan Ford",
    date: "2023-07-25",
    image: "/images/scalable-web.jpg",
  },
  {
    id: 8,
    title: "Understanding DevOps and CI/CD",
    content:
      "DevOps integrates development and operations to improve collaboration and productivity. Continuous Integration and Continuous Deployment.",
    author: "Fiona Green",
    date: "2023-08-30",
    image: "/images/devops-cicd.jpg",
  },
  {
    id: 9,
    title: "Benefits of Serverless Computing",
    content:
      "Serverless computing allows developers to build and run applications without managing infrastructure. This post discusses the benefits.",
    author: "George Harris",
    date: "2023-09-20",
    image: "/images/serverless.jpg",
  },
  {
    id: 10,
    title: "Introduction to Machine Learning",
    content:
      "Machine learning enables computers to learn from data and make predictions. This post introduces the basics of machine learning.",
    author: "Hannah Lee",
    date: "2023-10-05",
    image: "/images/machine-learning.jpg",
  },
];

import React, { useEffect, useState } from 'react';

const BlogPage = (props:any) => {

  const [apiData,setApiData] = useState([]);

  const API = 'https://fakestoreapi.com/products/'

  useEffect(()=>{
    fetch(API)
    .then(res=>res.json())
    .then(data=>setApiData(data))
  },[])
 
  return {

  }
};

export default BlogPage;

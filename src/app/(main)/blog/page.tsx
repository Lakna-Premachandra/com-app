"use client"

import React from 'react';
import { BlogData, BlogPost } from './Blog-data';
import Image from 'next/image';

const BlogPage: React.FC = () => {
  return (
    <div className="container mx-auto px-4 pb-8 pt-20">
      <div className="text-4xl sm:text-8xl font-bold py-8">
      <span className="text-outline-blue uppercase">Our </span>
      <span className="text-headercolor uppercase">Blog</span>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
        {BlogData.map((post: BlogPost) => (
          <div key={post.id} className="border flex flex-col gap-4 p-4 rounded shadow-md cursor-pointer">
            <Image src={post.image} alt={post.title} width={100} height={100} className="w-full aspect-[3/2] border rounded" />
            <div className="text-xl font-semibold leading-6">{post.title}</div>
            <div title={post.content} className="text-gray-700  line-clamp-3 leading-6">{post.content}</div>
            <div className="justify-between sm:flex">
              <div className="text-gray-600 font-bold">{post.author}</div>  
              <div className="text-green-600">{post.date}</div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default BlogPage;

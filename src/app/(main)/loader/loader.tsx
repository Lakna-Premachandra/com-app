// components/Loader.tsx
import Image from 'next/image';
import React from 'react';
import image from '../../../../public/images/slack.png';

const Loader: React.FC = () => {
    return (
        <div className="flex items-center justify-center min-h-screen">
            <Image className='animate-spin' width={100} src={image} alt='LOGO' />
        </div>
    );
};

export default Loader;

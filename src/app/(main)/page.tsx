import HeroSection from "./_components/hero-section";
import Projects from "./_components/projects";
import ServicesSection from "./_components/services-section";

export default function Home() {
  return (
    <>
      <HeroSection />
      <ServicesSection />
      <Projects />
    </>
  );
}

import React from 'react';
import { MdOutlineEmail, MdOutlinePhone } from 'react-icons/md';
import { SlLocationPin } from 'react-icons/sl';
import { GoClock } from "react-icons/go";

const contactInfo = [
  {
    icon: <SlLocationPin />,
    title: "Address",
    details: "No 10, Flower Road, Colombo 07",
  },
  {
    icon: <MdOutlinePhone />,
    title: "Call Us",
    details: "+94 112 333 666",
  },
  {
    icon: <MdOutlineEmail />,
    title: "Email Us",
    details: "info@ZephyrCode.com",
  },
  {
    icon: <GoClock />,
    title: "Open Hours",
    details: "Monday - Friday\n9:00AM - 05:00PM",
  },
];

const Contact: React.FC = () => {
  return (
    <div className="container px-4 sm:px-6 lg:px-8 ">
      <div className="text-4xl sm:text-8xl font-bold py-12">
        <span className="text-outline-blue uppercase">Contact&nbsp;</span>
        <span className="text-headercolor uppercase">Us</span>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-8 ">
        <div className="grid grid-cols-2 gap-4">
          {contactInfo.map((info, index) => (
            <div key={index} className="bg-slate-50 border rounded-xl p-4 flex flex-col gap-4">
              <div className='text-headercolor text-4xl'>{info.icon}</div>
              <h2 className="font-semibold tracking-wide text-xl">{info.title}</h2>
              <p className="font-extralight tracking-wide">{info.details}</p>
            </div>
          ))}
        </div>
        <div className="bg-slate-50  rounded-xl p-8">
          <form className="flex flex-col gap-4">
            <div className='flex  items-center gap-4'>
              <input
                type="text"
                id="name"
                className=" border border-gray-300 w-full p-3 tex-md focus:outline-none"
                placeholder="Your Name"
              />
              <input
                type="email"
                id="email"
                className=" border border-gray-300 w-full p-3 tex-md focus:outline-none"
                placeholder="Your Email"
              />
            </div>
            <div>
              <input
                type="text"
                id="subject"
                className=" border border-gray-300 w-full p-3 tex-md focus:outline-none"
                placeholder="Subject"
              />
            </div>
            <div>
              <textarea
                id="message"
                className=" border border-gray-300 w-full p-3 tex-md focus:outline-none"
                rows={4}
                placeholder="Message"
              ></textarea>
            </div>
            <div className='flex justify-center items-center'>
              <button
                type="submit"
                className=" bg-headercolor text-white font-medium  hover:bg-blue-700 px-4 py-2 rounded-[6px] tracking-wider"
              >
                Send Message
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Contact;
